# CHANGELOG
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.2.11]
### New Features
* Added fc_security.acf

## [0.2.7]
* clean up `example.cmd`, and delete `test.cmd`
### New features
* Set `asCheckClientIP` to 1, enabling access security checks of hostname/IP
  using DNS information

## [0.2.0]
### New Features
* Added mps_security.acf 
### Bugfixes
### Other changes

## [0.1.0]
### New Features
### Bugfixes
### Other changes
Renamed from e3-ess
