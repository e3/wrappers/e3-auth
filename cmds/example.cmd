# This module is part of the `essioc` "package", and should not be loaded explicitly (it will be loaded when `essioc` module is loaded).
require essioc

# Set IOC name
epicsEnvSet("IOCNAME", "my-IOC-name")

# The default access security policy is automatically loaded when you use a deployment system

# To use a different policy, override the variables defining the path and the .acf file
# This has to be done before loading `essioc`'s `common_config.iocsh`

# Set the path to your access control files
epicsEnvSet("PATH_TO_ASG_FILES", "/path/to/your/access-control-file")
# Specify access control file to use
epicsEnvSet("ASG_FILENAME", "your-acf-file.acf")
# Load the access control file into your IOC
asSetFilename("$(PATH_TO_ASG_FILES)/$(ASG_FILENAME)")

# Load your snippets and db-files and/or make your function calls here

iocInit()

